import datetime
import pytz

from django.db import models
from django.conf import settings

from month.models import MonthField
from django_fsm import FSMField, transition

from users.models import GENDER

utc=pytz.UTC

ACADEMIC_LEVELS = (
	(0, 'Artisan Certificate'),
	(1, 'Certifificate'),
	(2, 'Diploma'),
	(3, 'Higher Diploma'),
	(4, 'Bachelors Degree'),
	(5, 'Masters Degree'),
	(6, 'PhD.'),
	)

GRADES = (
		(1, 'A'),
		(2, 'A-'),
		(3, 'B+'),
		(4, 'B'),
		(5, 'B-'),
		(6, 'C+'),
		(7, 'C'),
		(8, 'C-'),
		(9, 'D+'),
		(10, 'D'),
		(11, 'D-'),
		(12, 'E'),
	)

ETHNIC_GROUPS = (
		('kikuyu', 'Kikuyu'),
		('meru', 'Meru'),
		('kisii', 'Kisii'),
		('embu', 'Embu'),
		('luhya', 'Luhya'),
		('swahili', 'Swahili'),
		('mijikenda', 'Mijikenda'),
		('luo', 'Luo'),
		('maasai', 'Maasai'),
		('turkana', 'Turkana'),
		('samburu', 'Samburu'),
		('kalenjin', 'Kalenjin'),
		('somali', 'Somali'),
		('el-molo', 'El Molo'),
		('boran', 'Boran'),
		('burji-dassenich', 'Burji Dassenich'),
		('gabbra', 'Gabbra'),
		('orma', 'Orma'),
		('sakuya', 'Sakuya'),
		('boni', 'Boni'),
		('wata', 'Wata'),
		('yaaka', 'Yaaka'),
		('daholo', 'Daholo'),
		('rendille', 'Rendille'),
		('galla', 'Galla'),
	)

REVIEW_CHOICES = (
    (True, u'Approve'),
    (False, u'Disapprove'),  
)

ACTION_CHOICES = (
    ('shortlist', 'Shortlisting'),
    ('appoint', 'Appointing'),  
)

# Create your models here.
class Qualification(models.Model):
	'''
	- level
	- course
	- institution
	- period

	'''
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='qualifications', on_delete=models.CASCADE)
	level = models.PositiveSmallIntegerField(choices=ACADEMIC_LEVELS)
	course = models.CharField(max_length=200)
	institution = models.CharField(max_length=200)
	period_from = MonthField("Commencement Period", null=True, blank=True)
	period_to = MonthField("Conclusion Period",blank=True, null=True)

	def __str__(self):
		return f'{self.get_level_display()} - {self.course}'

class Employment(models.Model):
	'''
	- organization
	- designation
	- period

	'''
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='employment_records', on_delete=models.CASCADE)
	designation = models.CharField(max_length=200, null=True, blank=True)
	organization = models.CharField(max_length=200, null=True, blank=True)
	period_from = MonthField("Starting Period", blank=True, null=True)
	period_to = MonthField("End Period",blank=True, null=True)
	current = models.BooleanField('I currently work here', default=False)

	def __str__(self):
		return f'{self.designation}'

class Department(models.Model):
	name = models.CharField(max_length=120)

	def __str__(self):
		return self.name

	def active_vacancies_count(self):
		vacancies = [vacancy for vacancy in self.vacancies.all() if vacancy.is_active]
		return len(vacancies)

class Vacancy(models.Model):
	'''
	- department
	- name
	- experience
	- requirements
	- timelines

	'''
	department = models.ForeignKey(Department, related_name='vacancies', on_delete=models.CASCADE)
	vacancy_number = models.CharField(max_length=20,  null=True)
	name = models.CharField(max_length=200)
	experience = models.PositiveSmallIntegerField()
	minimum_kcse_grade = models.IntegerField("Minimum K.C.S.E Grade", choices=GRADES, null=True)
	requirements = models.TextField()
	requires_compliances = models.BooleanField(default=True)
	opening_date = models.DateTimeField(auto_now_add=True)
	closing_date = models.DateTimeField()
	status = FSMField(default='active')
	meta_status = FSMField(default='awaiting_action')

	class Meta:
		verbose_name_plural='Vacancies'
		permissions = (
            ("can_view_active_vacancies", "Can view active vacancies"),
            ("can_view_closed_vacancies", "Can view closed vacancies"),
            ("can_add_vacancies", "Can add vacancies"),
            ("can_edit_vacancies", "Can edit vacancies"),
            ("can_delete_vacancies", "Can delete vacancies")
        )

	def __str__(self):
		return self.name

	@transition(field=status, source='active', target='closed')
	def close(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""

	@transition(field=status, source='closed', target='shortlisting_done')
	def set_shortlisted(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""

	@transition(field=status, source='shortlisting_done', target='appointing_done')
	def set_appointed(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""
	@transition(field=meta_status, source='awaiting_action', target='staged')
	def stage(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""
	@transition(field=meta_status, source='*', target='awaiting_full_consent')
	def set_uncertain(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""
	@transition(field=meta_status, source=['staged', 'awaiting_full_consent', 'awaiting_approval'], target='awaiting_approval')
	def set_awaiting_approval(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""
	@transition(field=meta_status, source='awaiting_approval', target='finalized')
	def finalize(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""
	@property
	def next_action(self):
		action = 'shortlist' if self.is_closed else 'appoint'
		return action

	@property
	def is_active(self):
		if self.status == 'active':
			return True
		return False

	@property
	def is_closed(self):
		if self.status == 'closed':
			return True
		return False

	@property
	def has_been_shortlisted(self):
		if self.status == 'shortlisting_done':
			return True
		return False

	@property
	def has_been_appointed(self):
		if self.status == 'appointing_done':
			return True
		return False

	@property
	def allows_staging(self):
		if self.applications.count():
			if self.status in ['closed', 'shortlisting_done'] and self.meta_status in ['awaiting_action', 'finalized']:
				return True
		return False

	@property
	def in_process(self):
		if self.applications.count():
			if self.meta_status not in ['awaiting_action', 'finalized']:
				return True
		return False

	@property
	def shortlisting_reviews_count(self):
		_shortlisting_reviews_count = self.reviews.filter(action='shortlist').count()
		return _shortlisting_reviews_count

	@property
	def appointment_reviews_count(self):
		_appointment_reviews_count = self.reviews.filter(action='appoint').count()
		return _appointment_reviews_count

	@property
	def shortlisting_approvals_count(self):
		_shortlisting_approvals_count = self.reviews.filter(approval=True, action='shortlist').count()
		return _shortlisting_approvals_count

	@property
	def shortlisting_disapprovals_count(self):
		_shortlisting_disapprovals_count = self.reviews.filter(approval=False, action='shortlist').count()
		return _shortlisting_disapprovals_count

	@property
	def appointment_approvals_count(self):
		_appointment_approvals_count = self.reviews.filter(approval=True, action='appoint').count()
		return _appointment_approvals_count

	@property
	def appointment_disapprovals_count(self):
		_appointment_disapprovals_count = self.reviews.filter(approval=False, action='appoint').count()
		return _appointment_disapprovals_count

class Review(models.Model):
	vacancy = models.ForeignKey(Vacancy, related_name='reviews', on_delete=models.CASCADE)
	user = models.ForeignKey('users.User', related_name='vacancy_reviews', on_delete=models.CASCADE)
	approval = models.BooleanField(choices=REVIEW_CHOICES, null=True)
	action = models.CharField(max_length=10, choices=ACTION_CHOICES, default='shortlist')
	is_final = models.BooleanField(default=False)	

class County(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return f'{self.name}'

class Subcounty(models.Model):
	county = models.ForeignKey(County, related_name='subcounties', on_delete=models.CASCADE)
	name = models.CharField(max_length=200)

	def __str__(self):
		return f'{self.name}'

class Ward(models.Model):
	county = models.ForeignKey(County, related_name='wards', on_delete=models.CASCADE, null=True)
	subcounty = models.ForeignKey(Subcounty, related_name='wards', on_delete=models.CASCADE)
	name = models.CharField(max_length=200)

	def __str__(self):
		return f'{self.name}'

class Sublocation(models.Model):
	county = models.ForeignKey(County, related_name='sublocations', on_delete=models.CASCADE, null=True)
	subcounty = models.ForeignKey(Subcounty, related_name='sublocations', on_delete=models.CASCADE)
	ward = models.ForeignKey(Ward, related_name='sublocations', on_delete=models.CASCADE)
	name = models.CharField(max_length=200)

	def __str__(self):
		return f'{self.name}'

class ProfessionalBody(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='professional_bodies', on_delete=models.CASCADE)
	name = models.CharField(max_length=150)
	registration_number = models.CharField(max_length=150)

class Referee(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='referees', null=True)
	full_name = models.CharField(max_length=300)
	occupation = models.CharField(max_length=200)
	mobile_number = models.CharField(max_length=13)
	email = models.EmailField(unique=True)
	period_known = models.CharField(max_length=100)

class Application(models.Model):
	'''
	INTERNAL FIELDS
	- personal details
		- Profile Info
		- Ethnicity
		- Disability Status		
	- other personal details
		- Criminal Conviction
		- Dismissal
	- abilities
	- compliance details

	EXTERNAL RELATIONS
	- locality
	- vacancy
	- qualifications
	- certifications
	- employment records
	- professional bodies
	- referees
	'''

	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='applications', on_delete=models.CASCADE)
	vacancy = models.ForeignKey(Vacancy, on_delete=models.SET_NULL, related_name='applications', null=True, blank=True)

	# Profile Info
	first_name = models.CharField(max_length=150)
	last_name = models.CharField(max_length=150)
	other_names = models.CharField(max_length=150)
	email = models.EmailField("Email Address")
	id_number = models.CharField("ID Number", max_length=10)
	date_of_birth = models.DateField()
	gender = models.CharField(max_length=1, choices=GENDER)
	phone = models.CharField("Phone Number", max_length=13)
	alternative_contact_name = models.CharField("Name of your Alternative Contact Person", max_length=200)
	alternative_contact_phone = models.CharField("Phone Number of your Alternative Contact Person", max_length=13)

	# KCSE Grade
	kcse_grade = models.IntegerField("K.C.S.E Grade", choices=GRADES)
	kcse_school = models.CharField("School where you attained your K.C.S.E", max_length=200)

	# Ethnicity
	ethnicity = models.CharField(max_length=20, choices=ETHNIC_GROUPS)

	# Disability Status
	disability_status = models.BooleanField("Are you living with disability?")
	nature_of_disability = models.TextField("Describe the nature of your disability", null=True, blank=True)
	disability_registration_number = models.CharField(max_length=20, null=True, blank=True)
	disability_registration_date = models.DateField(null=True, blank=True)

	# Conviction & Dismissal
	criminal_offence_conviction = models.BooleanField("Have you ever been convicted of a criminal offence?")
	conviction_description = models.TextField("Give the details appertaining to your conviction", null=True, blank=True)

	dismissal_from_employment = models.BooleanField("Have you ever been dismissed from employment?")
	dismissal_description = models.TextField("Give the details appertaining to your dismissal" ,null=True, blank=True)

	# Locality Details
	county = models.ForeignKey(County, related_name='applications', on_delete=models.SET_NULL, null=True)
	subcounty = models.ForeignKey(Subcounty, related_name='applications', on_delete=models.SET_NULL, null=True, blank=True)
	ward = models.ForeignKey(Ward, related_name='applications', on_delete=models.SET_NULL, null=True, blank=True)
	sublocation = models.ForeignKey(Sublocation, related_name='applications', on_delete=models.SET_NULL, null=True, blank=True)

	# Skills & Abilities
	skills = models.TextField("Indicate your skills and abilities", null=True, blank=True)

	# Compliances
	certificate_of_good_conduct_date_issued = models.DateField("D.C.I (Good Conduct) Certificate Date of Issue", null=True, blank=True)
	certificate_of_good_conduct_number = models.CharField("D.C.I (Good Conduct) Certificate Number", max_length=200, null=True, blank=True)

	helb_compliance_certificate_date_issued = models.DateField("H.E.L.B Compliance Certificate Date of Issue", null=True, blank=True)
	helb_compliance_certificate_number = models.CharField("H.E.L.B Compliance Certificate Number", max_length=200, null=True, blank=True)

	tax_compliance_certificate_date_issued = models.DateField("Tax Compliance Certificate Date of Issue", null=True, blank=True)
	tax_compliance_certificate_number = models.CharField("Tax Compliance Certificate Number", max_length=200, null=True, blank=True)

	crb_compliance_certificate_date_issued = models.DateField("C.R.B Compliance Certificate Date of Issue", null=True, blank=True)
	crb_compliance_certificate_number = models.CharField("C.R.B Compliance Certificate Number", max_length=200, null=True, blank=True)

	# Status
	status = FSMField(default='pending')
	meta_status = FSMField(default='awaiting_action')
	time_applied = models.DateTimeField(auto_now_add=True, null=True)

	class Meta:
		permissions = (
            ("can_view_applicants", "Can view applicants"),
            ("can_add_applications", "Can add applicants"),
            ("can_select_applicants", "Can select applicants"),
            ("can_shortlist", "Can shortlist"),
            ("can_approve_shortlist", "Can approve shortlist"),
            ("can_appoint", "Can appoint"),
            ("can_approve_appointment", "Can approve appointment"),
        )

	def __str__(self):
		return f'{self.first_name} {self.other_names} {self.last_name}'

	@transition(field=meta_status, source='awaiting_action', target='staged')
	def stage(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""

	@transition(field=meta_status, source='*', target='awaiting_action')
	def revert_meta(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""
	@transition(field=status, source='*', target='pending')
	def revert(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""

	@transition(field=status, source='pending', target='shortlisted')
	def shortlist(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""

	@transition(field=status, source='shortlisted', target='appointed')
	def appoint(self):
		"""
		This function may contain side-effects,
		like updating caches, notifying users, etc.
		The return value will be discarded.
		"""


	@property
	def age(self):
		if self.date_of_birth:
			today = datetime.date.today()
			return today.year - self.date_of_birth.year - ((today.month, today.day) < (self.date_of_birth.month, self.date_of_birth.day))
		return 0

	@property
	def experience(self):
		return 0

	@property
	def get_full_name(self):
		return f'{self.first_name} {self.other_names} {self.last_name}'