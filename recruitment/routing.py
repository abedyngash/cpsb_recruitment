from django.urls import path
from .consumers import (
	StagingConsumer, AssentingConsumer, ApprovalConsumer
	)

websocket_urlpatterns = [
	path('ws/recruitment/staging/', StagingConsumer.as_asgi()),
	path('ws/recruitment/assenting/', AssentingConsumer.as_asgi()),
	path('ws/recruitment/approval/', ApprovalConsumer.as_asgi()),
]