from django.urls import reverse
from django.test import TestCase, override_settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from recruitment.models import Qualification, Employment, Department, Vacancy


User = get_user_model()

class TestViews(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(username='00112233', id_number='00112233', password='321testing')
		group = Group.objects.get(name='ADMIN')
		self.user.groups.add(group)
		self.department = Department.objects.create(name='CPSB')
		self.client.login(username='00112233', password='321testing')

	def test_department_create(self):
		url = reverse('add-department')
		response = self.client.get(url)

		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed('recruitment/forms/modals/department/add_department.html')

	def test_department_update(self):
		url = reverse('update-department', args=[self.department.id])
		response = self.client.get(url)

		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed('recruitment/forms/modals/department/update_department.html')

	def test_department_delete(self):
		url = reverse('delete-department', args=[self.department.id])
		response = self.client.get(url)

		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed('recruitment/forms/modals/department/delete_department.html')

	@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
	def test_department_list(self):
		url = reverse('departments-list')
		response = self.client.get(url)

		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed('recruitment/lists/departments/all_departments.html')

