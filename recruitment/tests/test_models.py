from django.test import TestCase
from django.contrib.auth import get_user_model

from recruitment.models import Qualification, Employment, Department, Vacancy

User = get_user_model()

class TestModels(TestCase):
	def setUp(self):
		self.user = User.objects.create(id_number='00112233')
		self.qualification = Qualification.objects.create(
			user=self.user,
			level=4, 
			course='Computer Science',
			institution='DeKUT'
			)
		self.employment = Employment.objects.create(
			user=self.user,
			designation='ICT Officer'
			)
		self.department = Department.objects.create(name='CPSB')
		self.vacancy = Vacancy.objects.create(
			department=self.department,
			name='ICT Officer',
			experience=0,
			closing_date='2021-11-12 00:00:00'
			)

	def test_str_methods(self):
		self.assertEquals(str(self.qualification), f'{self.qualification.get_level_display()} - {self.qualification.course}')
		self.assertEquals(str(self.employment), self.employment.designation)
		self.assertEquals(str(self.department), self.department.name)
		self.assertEquals(str(self.vacancy), self.vacancy.name)