from apscheduler.schedulers.background import BackgroundScheduler
from .models import Vacancy

def close_vacancy(vacancy):
	vacancy.close()
	vacancy.save()

def start_scheduler():
	scheduler = BackgroundScheduler()
	for vacancy in Vacancy.objects.all():
		scheduler.add_job(close_vacancy, 'date', run_date=vacancy.closing_date, args=[vacancy])
	scheduler.start()
