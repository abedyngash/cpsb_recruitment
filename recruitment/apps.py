from django.apps import AppConfig
from django.db.models.signals import post_migrate

class RecruitmentConfig(AppConfig):
    name = 'recruitment'

    def ready(self):
    	from .signals import populate_models
    	from .scheduler import start_scheduler

    	post_migrate.connect(populate_models, sender=self)
    	start_scheduler()

