from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from .models import Application, Vacancy

class ApplicationExistsMixin:
    def dispatch(self, request, *args, **kwargs):
    	vacancy = Vacancy.objects.get(id=kwargs['pk'])
    	if Application.objects.filter(vacancy=vacancy, user=request.user).exists():
    		messages.error(self.request, 'You have already applied for this vacancy')
    		return redirect('active-vacancies-list')
    	else:
    		return super().dispatch(request, *args, **kwargs)

class ShortlistingReadyMixin:
    def dispatch(self, request, *args, **kwargs):
    	vacancy = Vacancy.objects.get(id=kwargs['pk'])
    	if not vacancy.is_closed:
    		messages.error(self.request, 'Vacancy not cleared for shortlisting')
    		return redirect('closed-vacancies-list')
    	else:
    		return super().dispatch(request, *args, **kwargs)

class AppointingReadyMixin:
    def dispatch(self, request, *args, **kwargs):
    	vacancy = Vacancy.objects.get(id=kwargs['pk'])
    	if not vacancy.has_been_shortlisted:
    		messages.error(self.request, 'Vacancy not cleared for appointment')
    		return redirect('closed-vacancies-list')
    	else:
    		return super().dispatch(request, *args, **kwargs)

class VacancyDeletableMixin:
    def dispatch(self, request, *args, **kwargs):
        vacancy = Vacancy.objects.get(id=kwargs['pk'])
        if vacancy.applications.all():
            messages.error(self.request, 'Vacancy cannot be deleted')
            return redirect(request.META.get('HTTP_REFERER', '/'))
        else:
            return super().dispatch(request, *args, **kwargs)