from django import forms
from django.forms.models import inlineformset_factory
from django.contrib.auth import get_user_model


from bootstrap_modal_forms.forms import BSModalModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Field, Fieldset, Div, MultiField, ButtonHolder, HTML

from .models import (
    Qualification, Employment, Department,
    Vacancy, ProfessionalBody, Application, Referee,
    County, Subcounty, Ward, Sublocation
)

User = get_user_model()

''' PERSONAL DETAILS FORMS '''


class QualificationForm(forms.ModelForm):
    class Meta:
        model = Qualification
        exclude = ['user', 'period_from', 'period_to']


class EmploymentForm(forms.ModelForm):
    class Meta:
        model = Employment
        exclude = ['user', 'period_from', 'period_to', 'current']


QualificationsFormSet = inlineformset_factory(
    User, Qualification, form=QualificationForm, extra=1, can_delete=True)
EmploymentFormSet = inlineformset_factory(
    User, Employment, form=EmploymentForm, extra=1, can_delete=True)


class CurrentEmploymentForm(forms.ModelForm):
    class Meta:
        model = Employment
        exclude = ['user', 'period_to']

    def __init__(self, *args, **kwargs):
        super(CurrentEmploymentForm, self).__init__(*args, **kwargs)

        self.fields['current'].label = 'Are you currently working?'

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Div(
                    Column(
                        Field('current', css_id="",
                              css_class="custom-control-input"),
                    ),
                    css_class="custom-control custom-switch custom-switch-md"
                ),
            ),
            Div(
                Row(
                    Column(
                        Field('designation', css_id="", css_class=""),
                    ),
                    Column(
                        Field('organization', css_id="", css_class=""),
                    ),
                ),
                Row(
                    Column(
                        Field('period_from', css_id="",
                              css_class="form-control", placeholder='YYYY'),
                    )
                ),
                css_class='current_employment_details'
            ),
            ButtonHolder(
                HTML(
                    '{% include "recruitment/forms/wizards/_buttons.html" %}'
                ),
            ),
        )


''' MODAL FORMS '''


class CountyForm(BSModalModelForm):
    class Meta:
        model = County
        fields = ['name']


class SubCountyForm(BSModalModelForm):
    class Meta:
        model = Subcounty
        fields = ['county', 'name']

    def __init__(self, *args, **kwargs):
        super(SubCountyForm, self).__init__(*args, **kwargs)
        self.fields['county'].widget.attrs['class'] = 'county_field'


class WardForm(BSModalModelForm):
    class Meta:
        model = Ward
        fields = ['county', 'subcounty', 'name']

    def __init__(self, *args, **kwargs):
        super(WardForm, self).__init__(*args, **kwargs)
        self.fields['county'].widget.attrs['class'] = 'county_field'
        self.fields['subcounty'].widget.attrs['class'] = 'subcounty_field'

        self.fields['subcounty'].queryset = Subcounty.objects.none()

        if 'county' in self.data:
            try:
                county_id = int(self.data.get('county'))
                self.fields['subcounty'].queryset = Subcounty.objects.filter(
                    county_id=county_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            try:
                self.fields['subcounty'].queryset = self.instance.county.subcounties.order_by(
                    'name')
            except Exception as e:
                pass


class SublocationForm(BSModalModelForm):
    class Meta:
        model = Sublocation
        fields = ['county', 'subcounty', 'ward', 'name']

    def __init__(self, *args, **kwargs):
        super(SublocationForm, self).__init__(*args, **kwargs)
        self.fields['county'].widget.attrs['class'] = 'county_field'
        self.fields['subcounty'].widget.attrs['class'] = 'subcounty_field'
        self.fields['ward'].widget.attrs['class'] = 'ward_field'

        self.fields['subcounty'].queryset = Subcounty.objects.none()
        self.fields['ward'].queryset = Ward.objects.none()

        if 'county' in self.data:
            try:
                county_id = int(self.data.get('county'))
                self.fields['subcounty'].queryset = Subcounty.objects.filter(
                    county_id=county_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            try:
                self.fields['subcounty'].queryset = self.instance.county.subcounties.order_by(
                    'name')
            except Exception as e:
                pass

        if 'subcounty' in self.data:
            try:
                subcounty_id = int(self.data.get('subcounty'))
                self.fields['ward'].queryset = Ward.objects.filter(
                    subcounty_id=subcounty_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            try:
                self.fields['ward'].queryset = self.instance.subcounty.wards.order_by(
                    'name')
            except Exception as e:
                pass


class DepartmentForm(BSModalModelForm):
    class Meta:
        model = Department
        fields = ['name']

    def __init__(self, *args, **kwargs):
        super(DepartmentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('name')
        )


class VacancyForm(BSModalModelForm):
    class Meta:
        model = Vacancy
        fields = ['department', 'vacancy_number', 'name',
                  'experience', 'requirements', 'closing_date']

    def __init__(self, *args, **kwargs):
        super(VacancyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Column(
                    Field('department')
                )
            ),
            Row(
                Column(
                    Field('vacancy_number')
                ),
                Column(
                    Field('name')
                )
            ),
            Row(
                Column(
                    Field('requirements')
                )
            ),
            Row(
                Column(
                    Field('experience')
                ),
                Column(
                    Field('closing_date')
                )
            )

        )


class QualificationsForm(BSModalModelForm):
    class Meta:
        model = Qualification
        fields = ['level', 'course', 'institution', 'period_from', 'period_to']

    def __init__(self, *args, **kwargs):
        super(QualificationsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Column(
                    Field('level')
                )
            ),
            Row(
                Column(
                    Field('course')
                )
            ),
            Row(
                Column(
                    Field('institution')
                )
            ),
            Row(
                Column(
                    Field('period_from', css_class='form-control',
                          placeholder='YYYY')
                ),
                Column(
                    Field('period_to', css_class='form-control',
                          placeholder='YYYY')
                )
            )

        )


class EmploymentsForm(BSModalModelForm):
    class Meta:
        model = Employment
        fields = ['organization', 'designation', 'period_from', 'period_to']

    def __init__(self, *args, **kwargs):
        super(EmploymentsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Column(
                    Field('organization')
                ),
                Column(
                    Field('designation')
                )
            ),
            Row(
                Column(
                    Field('period_from', css_class='form-control',
                          placeholder='YYYY')
                ),
                Column(
                    Field('period_to', css_class='form-control',
                          placeholder='YYYY')
                )
            )

        )


''' APPLICATION FORMS '''


class PersonalDetailsForm(forms.ModelForm):

    class Meta:
        model = Application
        fields = [
            'first_name', 'last_name', 'other_names',
            'email', 'id_number', 'date_of_birth', 'phone',
            'gender', 'ethnicity',
            'alternative_contact_name', 'alternative_contact_phone',
            'kcse_grade', 'kcse_school',
            'disability_status', 'nature_of_disability',
            'disability_registration_number', 'disability_registration_date',
            'criminal_offence_conviction', 'conviction_description',
            'dismissal_from_employment', 'dismissal_description',
            'county', 'subcounty', 'ward', 'sublocation'
        ]

    def __init__(self, *args, **kwargs):
        super(PersonalDetailsForm, self).__init__(*args, **kwargs)

        self.fields['county'].widget.attrs['class'] = 'county_field'
        self.fields['subcounty'].widget.attrs['class'] = 'subcounty_field'
        self.fields['ward'].widget.attrs['class'] = 'ward_field'

        # self.fields['subcounty'].queryset = Subcounty.objects.none()
        # self.fields['ward'].queryset = Ward.objects.none()

        if 'county' in self.data:
            try:
                county_id = int(self.data.get('county'))
                self.fields['subcounty'].queryset = Subcounty.objects.filter(
                    county_id=county_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            try:
                self.fields['subcounty'].queryset = self.instance.county.subcounties.order_by(
                    'name')
            except Exception as e:
                pass

        if 'subcounty' in self.data:
            try:
                subcounty_id = int(self.data.get('subcounty'))
                self.fields['ward'].queryset = Ward.objects.filter(
                    subcounty_id=subcounty_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            try:
                self.fields['ward'].queryset = self.instance.subcounty.wards.order_by(
                    'name')
            except Exception as e:
                pass

        if 'ward' in self.data:
            try:
                ward_id = int(self.data.get('ward'))
                self.fields['sublocation'].queryset = Sublocation.objects.filter(
                    ward_id=ward_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            try:
                self.fields['sublocation'].queryset = self.instance.wards.subcounties.order_by(
                    'name')
            except Exception as e:
                pass

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                'Basic Information',
                Row(
                    Column(
                        Field('first_name', css_id="",
                              css_class=""),
                    ),
                    Column(
                        Field('last_name', css_id="", css_class=""),
                    ),
                    Column(
                        Field('other_names',
                              css_id="", css_class=""),
                    ),
                ),
                Row(
                    Column(
                        Field('id_number', css_id="", css_class=""),
                    ),
                    Column(
                        Field('email', css_id="", css_class=""),
                    ),
                ),
                Row(
                    Column(
                        Field('date_of_birth',
                              css_id="", css_class=""),
                    ),
                    Column(
                        Field('phone', css_id="",
                              css_class="phone_input"),
                    ),
                ),
                Row(
                    Column(
                        Field('gender', css_id="", css_class=""),
                    ),
                    Column(
                        Field('ethnicity', css_id="",
                              css_class="form-control"),
                    ),
                ),
            ),
            Fieldset(
                'Alternative Contact Details',
                Row(
                    Column(
                        Field('alternative_contact_name',
                              css_id="", css_class=""),
                    ),
                    Column(
                        Field('alternative_contact_phone',
                              css_id="", css_class="phone_input"),
                    ),
                ),
            ),
            Fieldset(
                'Locality Details',
                Row(
                    Column(
                        Field('county', css_id="",
                              css_class="county_field"),
                    ),
                    Column(
                        Field('subcounty', css_id="",
                              css_class="subcounty_field"),
                    ),
                    Column(
                        Field('ward', css_id="",
                              css_class="ward_field"),
                    ),
                    Column(
                        Field('sublocation', css_id="",
                              css_class="sublocation_field"),
                    ),
                ),
            ),
            Fieldset(
                'K.C.S.E Details',
                Row(
                    Column(
                        Field('kcse_school',
                              css_id="", css_class=""),
                    ),
                    Column(
                        Field('kcse_grade', css_id="",
                              css_class=""),
                    ),
                ),
            ),
            Fieldset(
                'Disability Status',
                Row(
                    Div(
                        Column(
                            Field('disability_status', css_id="",
                                  css_class="custom-control-input"),
                        ),
                        css_class="custom-control custom-switch custom-switch-md"
                    ),
                ),
                Div(
                    Row(
                        Column(
                            Field('disability_registration_number',
                                  css_id="", css_class=""),
                        ),
                        Column(
                            Field('disability_registration_date',
                                  css_id="", css_class=""),
                        ),
                    ),
                    Row(
                        Column(
                            Field('nature_of_disability',
                                  css_id="", css_class=""),
                        )
                    ),
                    css_class='disability_additional_details'
                ),
            ),
            Fieldset(
                'Other Details',
                Row(
                    Column(
                        Div(
                            Column(
                                Field(
                                    'criminal_offence_conviction', css_id="", css_class="custom-control-input"),
                            ),
                            css_class="custom-control custom-switch custom-switch-md"
                        ),
                    ),
                    Column(
                        Div(
                            Column(
                                Field(
                                    'dismissal_from_employment', css_id="", css_class="custom-control-input"),
                            ),
                            css_class="custom-control custom-switch custom-switch-md"
                        ),
                    ),
                ),
                Div(
                    Row(
                        Column(
                            Field('conviction_description',
                                  css_id="", css_class=""),
                        )
                    ),
                    css_class='conviction_description'
                ),
                Div(
                    Row(
                        Column(
                            Field('dismissal_description',
                                  css_id="", css_class=""),
                        )
                    ),
                    css_class='dismissal_description'
                ),
            ),
            ButtonHolder(
                HTML(
                    '{% include "recruitment/forms/wizards/_buttons.html" %}'
                ),
            ),
        )

# QualificationsFormSet


class ProfessionalBodiesForm(forms.ModelForm):
    class Meta:
        model = ProfessionalBody
        exclude = ['user']


ProfessionalBodiesFormSet = inlineformset_factory(
    User, ProfessionalBody, form=ProfessionalBodiesForm, extra=1, can_delete=True)

# EmploymentFormSet


class AbilitiesForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ['skills']

    def __init__(self, *args, **kwargs):
        super(AbilitiesForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.layout = Layout(
            Row(
                Column(
                    Field('skills', css_id="", css_class=""),
                ),
            ),
            ButtonHolder(
                HTML(
                    '{% include "recruitment/forms/wizards/_buttons.html" %}'
                ),
            ),
        )


class CompliancesForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = [
            'certificate_of_good_conduct_date_issued', 'certificate_of_good_conduct_number',
            'helb_compliance_certificate_date_issued', 'helb_compliance_certificate_number',
            'tax_compliance_certificate_date_issued', 'tax_compliance_certificate_number',
            'crb_compliance_certificate_date_issued', 'crb_compliance_certificate_number',
        ]

    def __init__(self, *args, **kwargs):
        super(CompliancesForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()

        self.helper.layout = Layout(
            Row(
                Column(
                    Div(
                        HTML(
                            """<img src="{{STATIC_URL}}/img/dci_serial.png" class='img-fluid'>"""),
                        css_class='text-center'
                    ),
                    Field('certificate_of_good_conduct_number',
                          css_id="", css_class=""),
                ),
                Column(
                    Div(
                        HTML(
                            """<img src="{{STATIC_URL}}/img/dci_date.png" class='img-fluid'>"""),
                        css_class='text-center'
                    ),
                    Field('certificate_of_good_conduct_date_issued',
                          css_id="", css_class=""),
                ),
            ),
            Row(
                Column(
                    Div(
                        HTML(
                            """<img src="{{STATIC_URL}}/img/helb_serial.png" class='img-fluid'>"""),
                        css_class='text-center'
                    ),
                    Field('helb_compliance_certificate_number',
                          css_id="", css_class=""),
                ),
                Column(
                    Div(
                        HTML(
                            """<img src="{{STATIC_URL}}/img/helb_date.png" class='img-fluid'>"""),
                        css_class='text-center'
                    ),
                    Field('helb_compliance_certificate_date_issued',
                          css_id="", css_class=""),
                ),
            ),
            Row(
                Column(
                    Div(
                        HTML(
                            """<img src="{{STATIC_URL}}/img/tax_serial.png" class='img-fluid'>"""),
                        css_class='text-center'
                    ),
                    Field('tax_compliance_certificate_number',
                          css_id="", css_class=""),
                ),
                Column(
                    Div(
                        HTML(
                            """<img src="{{STATIC_URL}}/img/tax_date.png" class='img-fluid'>"""),
                        css_class='text-center'
                    ),
                    Field('tax_compliance_certificate_date_issued',
                          css_id="", css_class=""),
                ),
            ),
            Row(
                Column(
                    Field('crb_compliance_certificate_number',
                          css_id="", css_class=""),
                ),
                Column(
                    Field('crb_compliance_certificate_date_issued',
                          css_id="", css_class=""),
                ),
            ),
            ButtonHolder(
                HTML(
                    '{% include "recruitment/forms/wizards/_buttons.html" %}'
                ),
            ),
        )


class RefereesForm(forms.ModelForm):
    class Meta:
        model = Referee
        exclude = ['user', 'mobile_number', 'period_known']


# RefereesFormset Goes Here...
RefereesFormSet = inlineformset_factory(
    User, Referee, form=RefereesForm, extra=1, can_delete=True)
