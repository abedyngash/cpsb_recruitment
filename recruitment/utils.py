import africastalking

from django.shortcuts import render
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from .models import Ward, Subcounty, Sublocation

def create_wizard_formset_objects(_list, _model, _user):
	if _list:
		non_empty_list = [i for i in _list if bool(i)]
		for i in non_empty_list:
			if i['DELETE'] == True:
				i['id'].delete()
			else:
				objects_to_pop = [str('DELETE'), 'user']
				final_dict = dict([(k, v) for k,v in i.items() if k not in objects_to_pop])
				if final_dict['id'] == None:
					obj = _model.objects.create(user=_user, **final_dict)

def create_wizard_form_objects(_dict, _model, _user, **kwargs):
	obj = _model.objects.create(user=_user, **_dict, **kwargs)

def update_user_details(_dict, _model, _user):
	obj = _model.objects.filter(pk=_user.id).update(**_dict)

def load_subcounties(request):
	county_id = request.GET.get('county')
	subcounties = Subcounty.objects.filter(county_id=county_id).order_by('name')
	return render(request, 'recruitment/lists/locality/_subcounties_dropdown_list_options.html', {'subcounties': subcounties})

def load_wards(request):
	subcounty_id = request.GET.get('subcounty')
	wards = Ward.objects.filter(subcounty_id=subcounty_id).order_by('name')
	return render(request, 'recruitment/lists/locality/_wards_dropdown_list_options.html', {'wards': wards})

def load_sublocations(request):
	ward_id = request.GET.get('ward')
	sublocations = Sublocation.objects.filter(ward_id=ward_id).order_by('name')
	return render(request, 'recruitment/lists/locality/_sublocations_dropdown_list_options.html', {'sublocations': sublocations})

def email_applicant(applicant, action_type, assertion):
	email_context = {
		'applicant': applicant.get_full_name,
		'vacancy': applicant.vacancy		
	}

	if action_type == 'shortlist':
		mail_subject = 'Invitation to Interview' if assertion else f'Your Application for {applicant.vacancy}'
		template_name = 'users/email_shortlisted_applicant.html' if assertion else 'users/email_rejected_applicant.html'
		message = render_to_string(template_name, email_context)
		email_uid = urlsafe_base64_encode(force_bytes(applicant.email))

		email = EmailMessage(
			mail_subject,
			message,
			settings.DEFAULT_FROM_EMAIL,
		    to=[applicant.email],
		    reply_to=[settings.DEFAULT_REPLY_EMAIL],
		    headers={'Message-ID': email_uid},
		)

	elif action_type == 'appointment':
		mail_subject = f'Appointment to {applicant.vacancy}' if assertion else f'Your Application for {applicant.vacancy}'
		template_name = 'users/email_appointed_applicant.html' if assertion else 'users/email_rejected_applicant.html'
		message = render_to_string(template_name, email_context)
		email_uid = urlsafe_base64_encode(force_bytes(applicant.email))

		email = EmailMessage(
			mail_subject,
			message,
			settings.DEFAULT_FROM_EMAIL,
		    to=[applicant.email],
		    reply_to=[settings.DEFAULT_REPLY_EMAIL],
		    headers={'Message-ID': email_uid},
		)

	email.content_subtype = "html"
	email.send()

def send_sms(applicant, action_type):
	username = "nyeri_county"
	api_key = "d2d67126ff8a6a045c412cd267c11654da3657d7956cdbf085537ba422503145"

	africastalking.initialize(username, api_key)
	sms = africastalking.SMS

	message = f'{applicant.get_full_name} {applicant.vacancy}'
	sms.send(message, [applicant.phone], 'NYERI_CPSB', callback=on_finish)
