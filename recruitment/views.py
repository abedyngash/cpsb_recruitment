from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from django.utils.decorators import method_decorator
from django.db.models import Count, Q
from django.contrib.humanize.templatetags.humanize import naturalday

from django_tables2 import SingleTableView

from formtools.wizard.views import SessionWizardView
from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView, BSModalReadView, BSModalDeleteView

from users.decorators import allowed_users

from .forms import (
	# Forms
	QualificationsFormSet, QualificationsForm,
	EmploymentFormSet, EmploymentsForm,
	DepartmentForm, VacancyForm,

	# Locality Forms
	CountyForm, SubCountyForm, WardForm, SublocationForm,

	# Application Forms
	PersonalDetailsForm, ProfessionalBodiesFormSet, AbilitiesForm, CompliancesForm, RefereesFormSet,
	)
from .models import (
	Qualification, Employment, 
	Department, Vacancy, Review,
	County, Subcounty, Ward, Sublocation, 
	ProfessionalBody, Referee, Application,
	)
from .utils import create_wizard_formset_objects, create_wizard_form_objects
from .mixins import ApplicationExistsMixin, VacancyDeletableMixin
# from.tables import ApplicantTable

# Create your views here.
@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class DepartmentCreateView(BSModalCreateView):
    template_name = 'recruitment/forms/modals/department/add_department.html'
    form_class = DepartmentForm
    success_message = 'Success: Department was added.'

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class DepartmentUpdateView(BSModalUpdateView):
    model = Department
    template_name = 'recruitment/forms/modals/department/update_department.html'
    form_class = DepartmentForm
    success_message = 'Success: Department was updated.'

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class DepartmentListView(ListView):
	model = Department
	context_object_name = 'departments'
	template_name = 'recruitment/lists/departments/all_departments.html'

@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class DepartmentDeleteView(BSModalDeleteView):
    model = Department
    template_name = 'recruitment/forms/modals/department/delete_department.html'
    success_message = 'Success: Department was Deleted.'

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN', 'SECRETARY']), name='dispatch')
class VacancyCreateView(BSModalCreateView):
    template_name = 'recruitment/forms/modals/vacancies/add_vacancy.html'
    form_class = VacancyForm
    success_message = 'Success: Vacancy was added.'
    # success_url = reverse_lazy('home')
    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN', 'SECRETARY']), name='dispatch')
class VacancyUpdateView(BSModalUpdateView):
	model = Vacancy
	template_name = 'recruitment/forms/modals/vacancies/update_vacancy.html'
	form_class = VacancyForm
	success_message = 'Success: Vacancy was updated.'
	# success_url = reverse_lazy('home')
	def get_success_url(self):
		return self.request.META.get('HTTP_REFERER', None)

class VacancyRequirementsView(BSModalReadView):
	model = Vacancy
	template_name = 'recruitment/lists/vacancies/vacancy_requirements.html'

@allowed_users(['STAFF'])
def vacancy_details(request, pk):
	'''
	-> vacancy
	-> vacancy current status
	-> next milestone
	-> Based on vacancy meta_status
		-> Consensus Overview
		-> Final Approval
	-> Based on vacancy status
		-> Shortlisted Candidates
		-> Appointed Candidates
	'''
	vacancy = get_object_or_404(Vacancy, pk=pk)
	context = {'title': f'{vacancy} Details', 'vacancy': vacancy}

	current_status = vacancy.status
	current_meta_status = vacancy.meta_status

	milestones_stages = {
		'Application Stage': {
			'description': f'Vacancy accepting applications. Closed: {naturalday(vacancy.closing_date)}',
			'useful_links': {
				'View Applicants': {'url': reverse('applicants-list', args=[vacancy.pk]), 'icon': 'fas fa-eye'}
			}
		},
		'Shortlisting': {
			'description': f'Shortlisting done for this vacancy. Awaiting selection',
			'useful_links': {
				'View Applicants': {'url': reverse('applicants-list', args=[vacancy.pk]), 'icon': 'fas fa-eye'},
				'Shortlisted Candidates': {'url': reverse('shortlisted-candidates-list', args=[vacancy.pk]), 'icon': 'fas fa-long-arrow-alt-right'}
			}
		},
		'Selection/Appointment': {
			'description': f'Selection done for this vacancy. Vacancy has been finalized',
			'useful_links': {
				'View Applicants': {'url': reverse('applicants-list', args=[vacancy.pk]), 'icon': 'fas fa-eye'},
				'Shortlisted Candidates': {'url': reverse('shortlisted-candidates-list', args=[vacancy.pk]), 'icon': 'fas fa-long-arrow-alt-right'},
				'Selected Candidates': {'url': reverse('appointed-candidates-list', args=[vacancy.pk]), 'icon': 'fas fa-check-circle'}
			}
		},
	}

	context['shortlisting_reviews'] = Review.objects.filter(vacancy=vacancy, action='shortlist')
	context['appointment_reviews'] = Review.objects.filter(vacancy=vacancy, action='appoint')

	if vacancy.is_active:
		context['previous_milestones'] = None
		context['next_milestone'] = 'Vacancy Closure'
	elif vacancy.is_closed:
		context['previous_milestones'] = list(milestones_stages.items())[0:1]
		context['next_milestone'] = 'Shortlisting'
	elif vacancy.has_been_shortlisted:
		context['previous_milestones'] = list(milestones_stages.items())[0:2]
		context['next_milestone'] = 'Selection/Appointment'
	elif vacancy.has_been_appointed:
		context['previous_milestones'] = list(milestones_stages.items())
		context['next_milestone'] = None

	return render(request, 'recruitment/lists/vacancies/vacancy_details.html', context)


@method_decorator(allowed_users(['ADMIN', 'SECRETARY']), name='dispatch')
class VacancyDeleteView(VacancyDeletableMixin, BSModalDeleteView):
    model = Vacancy
    template_name = 'recruitment/forms/modals/vacancies/delete_vacancy.html'
    success_message = 'Success: Vacancy was deleted.'
    
    def get_success_url(self):
    	return reverse_lazy('home')
	
@method_decorator(allowed_users(['ALL']), name='dispatch')
class ActiveVacanciesListView(ListView):
	context_object_name = 'vacancies'
	
	def get_template_names(self):
		if self.request.user.is_applicant:
			return ['recruitment/lists/vacancies/active_vacancies_applicant.html']
		else:
			return ['recruitment/lists/vacancies/active_vacancies_staff.html']

	def get_queryset(self):
		active_vacancies = Vacancy.objects.filter(status='active')
		return active_vacancies

@method_decorator(allowed_users(['STAFF']), name='dispatch')
class ClosedVacanciesListView(ListView):
	context_object_name = 'vacancies'
	template_name = 'recruitment/lists/vacancies/closed_vacancies.html'

	def get_queryset(self):
		closed_vacancies = Vacancy.objects.filter(status='closed')
		return closed_vacancies

@method_decorator(allowed_users(['STAFF']), name='dispatch')
class ShortlistedVacanciesListView(ListView):
	context_object_name = 'vacancies'
	template_name = 'recruitment/lists/vacancies/shortlisted_vacancies.html'

	def get_queryset(self):
		shortlisted_vacancies = Vacancy.objects.filter(status='shortlisting_done').annotate(
			    shortlisted_candidates=Count('applications', filter=Q(applications__status='shortlisted')))
		return shortlisted_vacancies

@method_decorator(allowed_users(['STAFF']), name='dispatch')
class FinalizedVacanciesListView(ListView):
	context_object_name = 'vacancies'
	template_name = 'recruitment/lists/vacancies/finalized_vacancies.html'

	def get_queryset(self):
		finalized_vacancies = Vacancy.objects.filter(status='appointing_done').annotate(
			    appointed_candidates=Count('applications', filter=Q(applications__status='appointed')))
		return finalized_vacancies

@method_decorator(allowed_users(['APPLICANTS']), name='dispatch')
class QualificationsCreateView(BSModalCreateView):
	template_name = 'recruitment/forms/modals/qualifications/add_qualification.html'
	form_class = QualificationsForm

	def get_success_message(self, cleaned_data):
		return messages.success(self.request, 'Success: Qualification was added.')

	def get_success_url(self):
		return self.request.META.get('HTTP_REFERER', None)

	def form_valid(self, form):
		form.instance.user = self.request.user
		return super(QualificationsCreateView, self).form_valid(form)

@method_decorator(allowed_users(['APPLICANTS']), name='dispatch')
class QualificationsUpdateView(BSModalUpdateView):
	model = Qualification
	template_name = 'recruitment/forms/modals/qualifications/update_qualification.html'
	form_class = QualificationsForm

	def get_success_message(self, cleaned_data):
		return messages.success(self.request, 'Success: Qualification was updated.')

	def get_success_url(self):
		return self.request.META.get('HTTP_REFERER', None)

	def form_valid(self, form):
		form.instance.user = self.request.user
		return super(QualificationsUpdateView, self).form_valid(form)

method_decorator(allowed_users(['APPLICANTS']), name='dispatch')
class QualificationsDeleteView(BSModalDeleteView):
	model = Qualification
	template_name = 'recruitment/forms/modals/qualifications/delete_qualification.html'
	success_message = 'Success: Qualification was deleted.'

	def get_success_url(self):
		return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['APPLICANTS']), name='dispatch')
class EmploymentsCreateView(BSModalCreateView):
    template_name = 'recruitment/forms/modals/employments/add_employment.html'
    form_class = EmploymentsForm

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

    def form_valid(self, form):
    	form.instance.user = self.request.user
    	form.instance.current = False
    	return super(EmploymentsCreateView, self).form_valid(form)

    def get_success_message(self, cleaned_data):
    	return messages.success(self.request, 'Success: Employment record was added.')

@method_decorator(allowed_users(['APPLICANTS']), name='dispatch')
class EmploymentsUpdateView(BSModalUpdateView):
	model = Employment
	template_name = 'recruitment/forms/modals/employments/update_employment.html'
	form_class = EmploymentsForm

	def get_success_message(self, cleaned_data):
		return messages.success(self.request, 'Success: Record was updated.')

	def get_success_url(self):
		return self.request.META.get('HTTP_REFERER', None)

	def form_valid(self, form):
		form.instance.user = self.request.user
		form.instance.current = False
		return super(EmploymentsUpdateView, self).form_valid(form)

@method_decorator(allowed_users(['APPLICANTS']), name='dispatch')
class EmploymentsDeleteView(BSModalDeleteView):
    model = Employment
    template_name = 'recruitment/forms/modals/employments/delete_employment.html'
    success_message = 'Success: Employment record was deleted.'

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['STAFF']), name='dispatch')
class LocalityListView(ListView):
	context_object_name = 'counties'
	template_name = 'recruitment/lists/locality/locality_list.html'
	model = County

@method_decorator(allowed_users(['ADMIN', 'SECRETARIAT']), name='dispatch')
class CountyCreateView(BSModalCreateView):
    template_name = 'recruitment/forms/modals/locality/add_county.html'
    form_class = CountyForm
    success_message = 'Success: County was added.'
    # success_url = reverse_lazy('home')
    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN', 'SECRETARIAT']), name='dispatch')
class SubcountyCreateView(BSModalCreateView):
    template_name = 'recruitment/forms/modals/locality/add_subcounty.html'
    form_class = SubCountyForm
    
    def get_success_message(self, cleaned_data):
    	return messages.success(self.request, 'Success: Subcounty was added.')

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN', 'SECRETARIAT']), name='dispatch')
class WardCreateView(BSModalCreateView):
    template_name = 'recruitment/forms/modals/locality/add_ward.html'
    form_class = WardForm
    
    def get_success_message(self, cleaned_data):
    	return messages.success(self.request, 'Success: Ward was added.')

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN', 'SECRETARIAT']), name='dispatch')
class SublocationCreateView(BSModalCreateView):
    template_name = 'recruitment/forms/modals/locality/add_sublocation.html'
    form_class = SublocationForm
    
    def get_success_message(self, cleaned_data):
    	return messages.success(self.request, 'Success: Sublocation was added.')

    def get_success_url(self):
    	return self.request.META.get('HTTP_REFERER', None)

class VacancyApplicationView(LoginRequiredMixin, ApplicationExistsMixin, SessionWizardView):
	form_list = [
		('personal_details', PersonalDetailsForm),
		('qualifications_details', QualificationsFormSet),
		('professional_bodies', ProfessionalBodiesFormSet),
		('employment_records', EmploymentFormSet),
		('abilities', AbilitiesForm),
		('compliances', CompliancesForm),
		('referees', RefereesFormSet),
	]

	TEMPLATES = {
		'personal_details': 'recruitment/forms/wizards/applications/personal_details.html',
		'qualifications_details': 'recruitment/forms/wizards/applications/qualification_details.html',
		'professional_bodies': 'recruitment/forms/wizards/applications/professional_bodies.html',
		'employment_records': 'recruitment/forms/wizards/applications/employment_records.html',
		'abilities': 'recruitment/forms/wizards/applications/abilities.html',
		'compliances': 'recruitment/forms/wizards/applications/compliances.html',
		'referees': 'recruitment/forms/wizards/applications/referees.html'
	}

	def get_context_data(self, **kwargs):
	    context = super(VacancyApplicationView, self).get_context_data(**kwargs)
	    vacancy = Vacancy.objects.get(id=self.kwargs['pk']) 
	    context['vacancy'] = vacancy
	    return context

	def get_template_names(self):
		return [self.TEMPLATES[self.steps.current]]

	def get_form(self, step=None, data=None, files=None):
		form = super(VacancyApplicationView, self).get_form(step, data, files)
		if step is None:
			step = self.steps.current

		if step == 'qualifications_details':
			form.queryset = self.request.user.qualifications.all()
		elif step == 'professional_bodies':
			form.queryset = self.request.user.professional_bodies.all()
		elif step == 'employment_records':
			form.queryset = self.request.user.employment_records.all()
		elif step == 'referees':
			form.queryset = self.request.user.referees.all()

		return form

	def get_form_instance(self, step):
		if step == 'personal_details':
			# pass
			if self.request.user.applications.all().exists():
				return self.request.user.applications.all().first()
			return self.request.user
		elif step == 'abilities':
			return self.request.user.applications.first()
		elif step == 'compliances':
			return self.request.user.applications.first()

	def get_context_data(self, form, **kwargs):
		context = super(VacancyApplicationView, self).get_context_data(form=form, **kwargs)
		vacancy_id = self.kwargs['pk']
		context['skippable'] = True
		context['vacancy'] = Vacancy.objects.get(id=vacancy_id)
		if self.steps.current == 'personal_details' or self.steps.current == 'compliances':
			context.update({'skippable': False})
		return context

	def done(self, form_list, **kwargs):
		personal_details_cleaned_data = self.get_cleaned_data_for_step('personal_details')
		qualifications_details_cleaned_data = self.get_cleaned_data_for_step('qualifications_details')
		professional_bodies_cleaned_data = self.get_cleaned_data_for_step('professional_bodies')
		employment_records_cleaned_data = self.get_cleaned_data_for_step('employment_records')
		abilities_cleaned_data = self.get_cleaned_data_for_step('abilities')
		compliances_cleaned_data = self.get_cleaned_data_for_step('compliances')
		referees_cleaned_data = self.get_cleaned_data_for_step('referees')

		vacancy_id = kwargs['pk']
		vacancy = Vacancy.objects.get(id=vacancy_id)

		create_wizard_formset_objects(qualifications_details_cleaned_data, Qualification, self.request.user)
		create_wizard_formset_objects(professional_bodies_cleaned_data, ProfessionalBody, self.request.user)
		create_wizard_formset_objects(employment_records_cleaned_data, Employment, self.request.user)
		create_wizard_formset_objects(referees_cleaned_data, Referee, self.request.user)
		create_wizard_form_objects({**personal_details_cleaned_data, **abilities_cleaned_data, **compliances_cleaned_data}, Application, self.request.user, vacancy=vacancy)

		messages.success(self.request, 'Success: Application sent successfully')
		return redirect('home')

@method_decorator(allowed_users(['APPLICANTS']), name='dispatch')
class AppliedJobsView(ListView):
	context_object_name = 'applications'
	template_name = 'recruitment/lists/applications/my_jobs.html'

	def get_queryset(self):
		applications = Application.objects.all().filter(user=self.request.user)
		return applications

@method_decorator(allowed_users(['STAFF']), name='dispatch')
class ApplicantsListView(ListView):
	template_name = 'recruitment/lists/applications/applicants_list.html'
	context_object_name = 'applicants'
	# table_class = ApplicantTable

	def get_queryset(self):
		self.vacancy = get_object_or_404(Vacancy, pk=self.kwargs['pk'])
		return Application.objects.filter(vacancy=self.vacancy)

	def get_context_data(self, **kwargs):
	    context = super(ApplicantsListView, self).get_context_data(**kwargs)
	    context['vacancy'] = self.vacancy
	    context['next_action'] = 'shortlist' if self.vacancy.is_closed else 'appoint'
	    return context

@method_decorator(allowed_users(['STAFF']), name='dispatch')
class ShortlistedCandidatesListView(ListView):
	template_name = 'recruitment/lists/applications/candidates_list.html'
	context_object_name = 'applicants'
	# table_class = ApplicantTable

	def get_queryset(self):
		self.vacancy = get_object_or_404(Vacancy, pk=self.kwargs['pk'])
		return Application.objects.filter(vacancy=self.vacancy, status='shortlisted')

	def get_context_data(self, **kwargs):
	    context = super(ShortlistedCandidatesListView, self).get_context_data(**kwargs)
	    context['nature_of_action'] = 'Shortlisted'
	    context['next_action'] = 'shortlist' if self.vacancy.is_closed else 'appoint'
	    context['vacancy'] = self.vacancy
	    return context

@method_decorator(allowed_users(['STAFF']), name='dispatch')
class AppointedCandidatesListView(ListView):
	template_name = 'recruitment/lists/applications/candidates_list.html'
	context_object_name = 'applicants'
	# table_class = ApplicantTable

	def get_queryset(self):
		self.vacancy = get_object_or_404(Vacancy, pk=self.kwargs['pk'])
		return Application.objects.filter(vacancy=self.vacancy, status='appointed')

	def get_context_data(self, **kwargs):
	    context = super(AppointedCandidatesListView, self).get_context_data(**kwargs)
	    context['nature_of_action'] = 'Appointed'
	    context['next_action'] = 'shortlist' if self.vacancy.is_closed else 'appoint'
	    context['vacancy'] = self.vacancy
	    return context

@allowed_users(['BOARD_MEMBERS'])
def awaiting_room(request, pk, action_type):
	vacancy = get_object_or_404(Vacancy, pk=pk)
	vacancy_reviews = vacancy.reviews.all()
	user_review_exists = request.user.vacancy_reviews.filter(vacancy=vacancy, action=action_type).exists()
	staged_candidates = vacancy.applications.filter(meta_status='staged')

	context = {
		'vacancy': vacancy,
		'vacancy_reviews': vacancy_reviews,
		'user_review_exists': user_review_exists,
		'staged_candidates': staged_candidates,
		'action_type': action_type
	}

	return render(request, 'recruitment/lists/applications/awaiting_room.html', context)