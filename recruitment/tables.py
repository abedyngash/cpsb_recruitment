import django_tables2 as tables
from django_tables2_column_shifter.tables import (
    ColumnShiftTableBootstrap4,
)
from django_tables2.utils import A


from .models import Application

SELECT_COLUMN = '''
   	{% if perms.recruitment.can_select_applicants %}
		<input type="checkbox" name="selected_candidates" value="{{record.id}}">
		{% else %}
		{{forloop.counter}}
	{% endif %}
'''

QUALIFICATIONS = '''
	{% for qualification in record.user.qualifications.all  %}
		- {{qualification.get_level_display}} in {{qualification.course}}<br>
	{% endfor %}
'''


class ApplicantTable(ColumnShiftTableBootstrap4):
	column_default_show = [
		'checkbox_column', 'get_full_name', 'id_number', 'age', 'kcse_grade', 
		'qualifications', 'county', 'subcounty', 'ward', 'phone'
	]

	get_full_name = tables.Column(verbose_name='Full Name', accessor='get_full_name', order_by='first_name')
	age = tables.Column(verbose_name='Age', accessor='age', order_by='date_of_birth')
	qualifications = tables.TemplateColumn(QUALIFICATIONS)
	# select_column = tables.TemplateColumn(SELECT_COLUMN, orderable=False)
	checkbox_column = tables.CheckBoxColumn(accessor='pk', attrs={
		'td__input': {'class':'checkboxinput', 'name': "selected_candidates"}, 'th__input': {'class': 'checkboxinput', 'id':'select-all', 'name': 'users'},}
	)

	class Meta:
	    model = Application
	    template_name = "django_tables2_column_shifter/table.html"
	    exclude = [
		    'id','user', 'vacancy',
		    'first_name', 'last_name', 'other_names',
		    'date_of_birth',
		    'kcse_school',
			'disability_status',
			'nature_of_disability',
			'disability_registration_number',
			'disability_registration_date',
			'criminal_offence_conviction',
			'conviction_description',
			'dismissal_from_employment',
			'dismissal_description',
			'skills',
			'certificate_of_good_conduct_number',
			'helb_compliance_certificate_number',
			'tax_compliance_certificate_number',
			'crb_compliance_certificate_number',
			'status',
			'time_applied',
	    ]
	    sequence = [
	    	'checkbox_column', 'get_full_name', 'id_number', 'age', 'kcse_grade', 
			'qualifications', 'county', 'subcounty', 'ward', 'phone',
	    ]

	    attrs = {'class': 'table-sm'}