import json
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async

from django.core import serializers
from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder

from .models import Application, Vacancy, Review

@database_sync_to_async
def _user_is_staff(user):
	return user.groups.filter(name='STAFF').exists()

@database_sync_to_async
def _user_in_group(user, group_name):
	return user.groups.filter(name=group_name).exists()

@database_sync_to_async
def _get_vacancy(pk):
	vacancy = Vacancy.objects.get(pk=pk)
	return vacancy

@database_sync_to_async
def _get_serialized_vacancy(pk):
	vacancy = Vacancy.objects.get(pk=pk)
	next_action = f', "next_action": "{vacancy.next_action}"'
	data = serializers.serialize('json', [vacancy])

	mod_data = data[:len(data) - 2]
	mod_data += next_action
	mod_data += '}]'

	return mod_data

@database_sync_to_async
def _check_if_user_review_exists(user, vacancy, action_type):
	return Review.objects.filter(
		vacancy=vacancy,
		user= user,
		action=action_type
	).exists()

@database_sync_to_async
def _get_shortlisted_candidates(vacancy_pk):
	vacancy = Vacancy.objects.get(pk=vacancy_pk)
	shortlisted_candidates = vacancy.applicants.filter(status='shortlisted')
	data = serializers.serialize('json', list(shortlisted_candidates))
	return data

@database_sync_to_async
def _get_candidates(pks, vacancy):
	candidates = Application.objects.filter(pk__in=pks, vacancy=vacancy)
	data = serializers.serialize('json', list(candidates))
	return data

@database_sync_to_async
def _get_shortlisted_objects(pks, vacancy):
	candidates = Application.objects.filter(pk__in=pks, vacancy=vacancy)
	return candidates

@database_sync_to_async
def _approve_or_disapprove(vacancy, user, action, consent):
	review, created = Review.objects.get_or_create(vacancy=vacancy, 
		user=user, action=action, approval=consent)

class StagingConsumer(AsyncJsonWebsocketConsumer):
	pass
	'''
	action_type
	vacancy_id
	candidate_ids
	'''

	async def connect(self):
		self.room_group_name = 'STAGING_ROOM'
		user_is_staff = await _user_is_staff(self.scope['user'])

		if user_is_staff:
			await self.channel_layer.group_add(
				self.room_group_name,
				self.channel_name
				)
			await self.accept()

	async def disconnect(self, close_code):
		await self.channel_layer.group_discard(
			self.room_group_name,
			self.channel_name
			)

	async def receive_json(self, content, **kwargs):
		candidate_ids = content['candidates']
		vacancy_id = int(content['vacancy'])
		action_type = content['action_type']

		serialized_vacancy = json.loads(await _get_serialized_vacancy(vacancy_id))
		vacancy = await _get_vacancy(vacancy_id)
		candidates = json.loads(await _get_candidates(candidate_ids, vacancy))
		review_exists = await _check_if_user_review_exists(self.scope['user'], vacancy, action_type)

		# self._revert_vacancy(vacancy)
		await self._stage_vacancy(vacancy)
		await self._stage_candidates(candidate_ids, vacancy)

		if await _user_in_group(self.scope['user'], 'SECRETARY'):
			await self.channel_layer.group_send(self.room_group_name, {
				'type': 'stage_candidates',
				'candidates': candidates,
				'vacancy': serialized_vacancy,
				'review_exists': review_exists
			})


	async def stage_candidates(self, event):
		if not await _user_in_group(self.scope['user'], 'BOARD_CHAIRPERSON'):
			await self.send_json({
	            "msg_type": 'stage_candidates',
	            "candidates": event["candidates"],
	            "vacancy": event["vacancy"][0],
	            'review_exists': event['review_exists']
	        })

	async def stage_meta(self, event):
		if self.channel_name == event['sender_channel_name']:
			await self.send_json({
	            "msg_type": 'stage_vacancy',
	          	"message": event['message']
	        })

	@database_sync_to_async
	def _stage_vacancy(self, vacancy):
		try:
			vacancy.meta_status = 'staged'
			vacancy.save()
			# vacancy.refresh_from_db()
		except Exception as e:
			self.channel_layer.group_send(self.room_group_name, {
				'type': 'stage_meta',
				'message': f'Errors encountered in staging the vacancy: -> {e}',
				'sender_channel_name': self.channel_name
			})

	@database_sync_to_async
	def _stage_candidates(self, candidate_pks, vacancy):
		try:
			candidates = Application.objects.filter(pk__in=candidate_pks, vacancy=vacancy)
			for candidate in candidates:
				candidate.stage()
				candidate.save()
				# candidate.refresh_from_db()
		except Exception as e:
			self.channel_layer.group_send(self.room_group_name, {
				'type': 'stage_meta',
				'message': f'Errors encountered in staging the candidates: -> {e}',
				'sender_channel_name': self.channel_name
			})

class AssentingConsumer(AsyncJsonWebsocketConsumer):
	pass
	'''
	action_type
	vacancy_id
	candidate_ids
	feedback_type
	board_member_ids
	'''

	async def connect(self):
		self.counter = 0
		self.room_group_name = 'ASSENTING_ROOM'
		user_is_staff = await _user_is_staff(self.scope['user'])

		if user_is_staff:
			await self.channel_layer.group_add(
				self.room_group_name,
				self.channel_name
				)
			await self.accept()

	async def disconnect(self, close_code):
		await self.channel_layer.group_discard(
			self.room_group_name,
			self.channel_name
			)

	async def receive_json(self, content, **kwargs):
		user = self.scope['user']
		action_type = content['action_type']
		vacancy_id = int(content['vacancy'])
		candidate_ids = content['candidates']
		consent = content['consent']

		serialized_vacancy = json.loads(await _get_serialized_vacancy(vacancy_id))
		vacancy = await _get_vacancy(vacancy_id)
		candidates = json.loads(await _get_candidates(candidate_ids, vacancy))
		review_exists = await _check_if_user_review_exists(self.scope['user'], vacancy, action_type)

		if await _user_in_group(self.scope['user'], 'BOARD_MEMBERS'):
			await _approve_or_disapprove(vacancy, user, action_type, consent)
			self.counter += 1
			await self._change_vacancy_meta_status(vacancy, 'awaiting_full_consent')

			# Send notification that B.M has approved
			await self.channel_layer.group_send(self.room_group_name, {
				'type': 'notify_members',
				'member': f'{user.first_name} {user.last_name}',
				'consent': consent,
				'vacancy_id': vacancy_id,
				'action_type': action_type,
				'sender_channel_name': self.channel_name
			})

		if self.counter >= settings.MEMBERS_QUORUM:
			await self._change_vacancy_meta_status(vacancy, 'awaiting_approval')			
			candidates = await _get_candidates(candidate_ids, vacancy)
			consentees = await self._get_approvers(vacancy, action_type)

			# Prompt chairperson for action
			await self.channel_layer.group_send(self.room_group_name, {
				'type': 'approve_action',
				'consentees': consentees,
				'action_type': action_type,
				'vacancy': serialized_vacancy,
				'candidates' : candidates
			})

	async def notify_members(self, event):
		if self.channel_name == event['sender_channel_name']:
			await self.send_json({
				"sender": True,
				"msg_type": "notify_members",
				"message": f'Action recorded successfully',
				"vacancy_id": event['vacancy_id'],
				"action_type": event['action_type'],
				})
		else:
			action = 'approved' if event['consent'] else 'disapproved'
			await self.send_json({
				"sender": False,
				"msg_type": "notify_members",
				"message": f"Member {event['member']} has {action} the {event['action_type']}",
				})
			
	async def approve_action(self, event):
		if await _user_in_group(self.scope['user'], 'BOARD_CHAIRPERSON'):
			await self.send_json({
				"msg_type": "approve_action",
				"consentees": event['consentees'],
				"vacancy": event['vacancy'][0],
				"candidates": event['candidates'],
				})

	@database_sync_to_async
	def _get_approvers(self, vacancy, action_type):
		approvers = vacancy.reviews.filter(approval=True, action=action_type).values('user__first_name', 'user__last_name')
		data = json.dumps(list(approvers), cls=DjangoJSONEncoder)
		return data

	@database_sync_to_async
	def _change_vacancy_meta_status(self, vacancy, state):
		vacancy.meta_status = state
		vacancy.save()
		# vacancy.refresh_from_db()

class ApprovalConsumer(AsyncJsonWebsocketConsumer):
	pass
	'''
	action_type
	vacancy_id
	candidate_ids
	feedback_type
	'''

	async def connect(self):
		self.room_group_name = 'APPROVING_ROOM'
		user_is_staff = await _user_is_staff(self.scope['user'])

		if user_is_staff:
			await self.channel_layer.group_add(
				self.room_group_name,
				self.channel_name
				)
			await self.accept()

	async def disconnect(self, close_code):
		await self.channel_layer.group_discard(
			self.room_group_name,
			self.channel_name
			)

	async def receive_json(self, content, **kwargs):
		user = self.scope['user']
		vacancy_id = content['vacancy_id']
		candidate_pks = content['candidates_pks']
		action_type = content['action_type']
		approval = content['approval']

		vacancy = await _get_vacancy(vacancy_id)

		await _approve_or_disapprove(vacancy ,user, action_type, approval)
		
		if approval:
			if action_type == 'shortlist':
				await self._shortlist_candidates(candidate_pks, vacancy)
				await self._shortlist_vacancy(vacancy_id)
			elif action_type == 'appoint':
				await self._appoint_candidates(candidate_pks, vacancy)
				await self._set_vacancy_appointed(vacancy_id)

		action = 'approved' if approval else 'disapproved'
		review_type = 'shortlist' if action_type == 'shortlist' else 'appointment'

		await self.channel_layer.group_send(self.room_group_name, {
			'type': 'notify_approval',
			'sender_channel_name': self.channel_name,
			'vacancy_id': vacancy_id,
			'message': f'Member {user.first_name} {user.last_name} has {action} the {review_type} for vacancy {vacancy.name}'
			# 'member': f'{user.first_name} {user.last_name}',
			# 'vacancy': f'{vacancy.name}'
		})


	async def notify_approval(self, event):
		if self.channel_name == event['sender_channel_name']:
			message = 'Approval recorded successfully'
		else:
			message = event['message']
		
		await self.send_json({
			"message": message,
			"vacancy_id": event['vacancy_id'],
		})

	@database_sync_to_async
	def _shortlist_vacancy(self, pk):
		vacancy = Vacancy.objects.get(pk=pk)
		vacancy.set_shortlisted()
		vacancy.finalize()
		vacancy.save()
		vacancy.refresh_from_db()

	@database_sync_to_async
	def _set_vacancy_appointed(self, pk):
		vacancy = Vacancy.objects.get(pk=pk)
		vacancy.set_appointed()
		vacancy.finalize()
		vacancy.save()
		vacancy.refresh_from_db()

	@database_sync_to_async
	def _shortlist_candidates(self, pks, vacancy):
		candidates = Application.objects.filter(pk__in=pks, vacancy=vacancy)
		for candidate in candidates:
			candidate.meta_status = 'awaiting_action'
			candidate.shortlist()
			candidate.save()

	@database_sync_to_async
	def _appoint_candidates(self, pks, vacancy):
		candidates = Application.objects.filter(pk__in=pks, vacancy=vacancy)
		for candidate in candidates:
			candidate.meta_status = 'awaiting_action'
			candidate.appoint()
			candidate.save()