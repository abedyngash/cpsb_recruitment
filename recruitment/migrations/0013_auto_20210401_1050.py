# Generated by Django 3.1.7 on 2021-04-01 07:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0012_auto_20210326_1603'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='application',
            options={'permissions': (('can_view_applicants', 'Can view applicants'), ('can_add_applications', 'Can add applicants'), ('can_select_applicants', 'Can select applicants'), ('can_shortlist', 'Can shortlist'), ('can_appoint', 'Can appoint'))},
        ),
        migrations.AlterModelOptions(
            name='vacancy',
            options={'permissions': (('can_view_active_vacancies', 'Can view active vacancies'), ('can_view_closed_vacancies', 'Can view closed vacancies'), ('can_add_vacancies', 'Can add vacancies'), ('can_edit_vacancies', 'Can edit vacancies'), ('can_delete_vacancies', 'Can delete vacancies')), 'verbose_name_plural': 'Vacancies'},
        ),
    ]
