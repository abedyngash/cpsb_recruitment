from django.urls import path
from .views import (
	VacancyApplicationView,
	DepartmentCreateView, DepartmentListView, DepartmentUpdateView, DepartmentDeleteView,
	VacancyCreateView, ActiveVacanciesListView, ClosedVacanciesListView, 
	ShortlistedVacanciesListView, FinalizedVacanciesListView,
	VacancyUpdateView, VacancyRequirementsView, VacancyDeleteView, vacancy_details,
	QualificationsCreateView, QualificationsUpdateView, QualificationsDeleteView,
	EmploymentsCreateView, EmploymentsUpdateView,EmploymentsDeleteView,
	LocalityListView,
	CountyCreateView, SubcountyCreateView, WardCreateView, SublocationCreateView,
	AppliedJobsView,
	ApplicantsListView, ShortlistedCandidatesListView, AppointedCandidatesListView,
	awaiting_room
	)

from .utils import load_subcounties, load_wards, load_sublocations

urlpatterns = [

	path('departments/list/', DepartmentListView.as_view(), name='departments-list'),
	path('departments/add/', DepartmentCreateView.as_view(), name='add-department'),
	path('department/<int:pk>/update/', DepartmentUpdateView.as_view(), name='update-department'),
	path('department/<int:pk>/delete/', DepartmentDeleteView.as_view(), name='delete-department'),

	path('vacancy/add/', VacancyCreateView.as_view(), name='add-vacancy'),
	path('vacancies/<int:pk>/details/', VacancyRequirementsView.as_view(), name='vacancy-requirements'),
	path('vacancies/<int:pk>/update/', VacancyUpdateView.as_view(), name='update-vacancy'),
	path('vacancies/<int:pk>/delete/', VacancyDeleteView.as_view(), name='delete-vacancy'),
	path('vacancies/<int:pk>/', vacancy_details, name='vacancy-details'),
	
	path('vacancies/list/active/', ActiveVacanciesListView.as_view(), name='active-vacancies-list'),
	path('vacancies/list/closed/', ClosedVacanciesListView.as_view(), name='closed-vacancies-list'),
	path('vacancies/list/shortlisted/', ShortlistedVacanciesListView.as_view(), name='shortlisted-vacancies-list'),
	path('vacancies/list/finalized/', FinalizedVacanciesListView.as_view(), name='finalized-vacancies-list'),

	path('qualifications/add/', QualificationsCreateView.as_view(), name='add-qualification'),
	path('qualifications/<int:pk>/update/', QualificationsUpdateView.as_view(), name='update-qualification'),
	path('qualifications/<int:pk>/delete/', QualificationsDeleteView.as_view(), name='delete-qualification'),

	path('employments/add/', EmploymentsCreateView.as_view(), name='add-employment'),
	path('employments/<int:pk>/update/', EmploymentsUpdateView.as_view(), name='update-employment'),
	path('employments/<int:pk>/delete/', EmploymentsDeleteView.as_view(), name='delete-employment'),

	path('my-applied-jobs/', AppliedJobsView.as_view(), name='my-applied-jobs'),

	path('vacancies/<int:pk>/apply/', VacancyApplicationView.as_view(), name='apply'),
	path('vacancies/<int:pk>/applicants/', ApplicantsListView.as_view(), name='applicants-list'),
	path('vacancies/<int:pk>/applicants/shortlisted-candidates/', ShortlistedCandidatesListView.as_view(), name='shortlisted-candidates-list'),
	path('vacancies/<int:pk>/applicants/appointed-candidates/', AppointedCandidatesListView.as_view(), name='appointed-candidates-list'),
	path('vacancies/<int:pk>/awaiting_room/<str:action_type>/', awaiting_room, name='awaiting_room'),

	path('locality/list/', LocalityListView.as_view(), name='locality-list'),

	path('county/add/', CountyCreateView.as_view(), name='add-county'),
	path('subcounty/add/', SubcountyCreateView.as_view(), name='add-subcounty'),
	path('ward/add/', WardCreateView.as_view(), name='add-ward'),
	path('sublocation/add/', SublocationCreateView.as_view(), name='add-sublocation'),

	path('ajax/load-subcounties/', load_subcounties, name='ajax_load_subcounties'),
	path('ajax/load-wards/', load_wards, name='ajax_load_wards'),
	path('ajax/load-sublocations/', load_sublocations, name='ajax_load_sublocations'),
]