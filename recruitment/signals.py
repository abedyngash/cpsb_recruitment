from django.contrib.auth.models import Group, Permission

def populate_models(sender, **kwargs):
	board_chairperson, created = Group.objects.get_or_create(name ='BOARD_CHAIRPERSON')
	board_members, created = Group.objects.get_or_create(name ='BOARD_MEMBERS')
	secretary, created = Group.objects.get_or_create(name='SECRETARY')
	secretariat, created = Group.objects.get_or_create(name='SECRETARIAT')
	admin, created = Group.objects.get_or_create(name='ADMIN')
	staff, created = Group.objects.get_or_create(name='STAFF')

	board_chairperson_permissions = Permission.objects.filter(
			codename__in=[
				'can_approve_shortlist',
				'can_approve_appointment',
			]
		)

	board_members_permissions = Permission.objects.filter(
			codename__in=[
				'can_view_active_vacancies',
				'can_view_closed_vacancies',
				'can_view_applicants',
				'can_shortlist',
				'can_appoint',
			]
		)

	secretary_permissions = Permission.objects.filter(
			codename__in=[
				'can_view_active_vacancies',
				'can_view_closed_vacancies',
				'can_view_applicants',
				'can_shortlist',
				'can_appoint',
				'can_add_vacancies',
				'can_edit_vacancies',
				'can_delete_vacancies',
				'can_select_applicants',
			]
		)

	secretariat_permissions = Permission.objects.filter(
			codename__in=[
				'can_view_applicants',
				'can_add_applications',
				'can_view_active_vacancies',
				'can_view_closed_vacancies',
			]
		)

	admin_permissions = Permission.objects.get(codename='has_admin_privileges')
	staff_permissions = Permission.objects.get(codename='has_staff_privileges')

	board_chairperson.permissions.add(*board_chairperson_permissions)
	board_members.permissions.add(*board_members_permissions)
	secretary.permissions.add(*secretary_permissions)
	secretariat.permissions.add(*secretariat_permissions)
	admin.permissions.add(admin_permissions)
	staff.permissions.add(staff_permissions)
