
var county_id = $(".county_field").val();
var subcounty_id = $(".subcounty_field").val();
var ward_id = $(".ward_field").val();

var subcounties_url = $("#form").attr("data-subcounties-url");
var wards_url = $("#form").attr("data-wards-url");
var sublocations_url = $("#form").attr("data-sublocations-url");

var county_func = function(url, county_id) {
	$.ajax({                       // initialize an AJAX request
	  url: url,                    // set the url of the request (= localhost:8000/hr/ajax/load-cities/)
	  data: {
	    'county': county_id       // add the country id to the GET parameters
	  },
	  success: function (data) {   // `data` is the return of the `load_cities` view function
	    $(".subcounty_field").html(data);  // replace the contents of the city input with the data that came from the server
	  }
	});    
}

var subcounty_func = function(url, subcounty_id) {
	$.ajax({                       // initialize an AJAX request
	  url: url,                    // set the url of the request (= localhost:8000/hr/ajax/load-cities/)
	  data: {
	    'subcounty': subcounty_id       // add the country id to the GET parameters
	  },
	  success: function (data) {   // `data` is the return of the `load_cities` view function
	    $(".ward_field").html(data);  // replace the contents of the city input with the data that came from the server
	  }
	});    
}

var ward_func = function(url, ward_id) {
	$.ajax({                       // initialize an AJAX request
	  url: url,                    // set the url of the request (= localhost:8000/hr/ajax/load-cities/)
	  data: {
	    'ward': ward_id       // add the country id to the GET parameters
	  },
	  success: function (data) {   // `data` is the return of the `load_cities` view function
	    $(".sublocation_field").html(data);  // replace the contents of the city input with the data that came from the server
	  }
	});    
}

county_func(subcounties_url, county_id);
subcounty_func(wards_url, subcounty_id);
ward_func(sublocations_url, ward_id);

$(".county_field").change(function () {
	var county_id = $(this).val(); 
	county_func(subcounties_url, county_id);
});

$(".subcounty_field").change(function () {
	var subcounty_id = $(this).val(); 
	subcounty_func(wards_url, subcounty_id);
});

$(".ward_field").change(function () {
	var ward_id = $(this).val(); 
	ward_func(sublocations_url, ward_id);
});