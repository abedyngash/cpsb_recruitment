from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import Group, Permission

USER_TYPE_CHOICES = {
	0: 'Superuser',
	1: 'System Administrator',
	2: 'Board Chairperson',
	3: 'Board Vice-Chair',
	4: 'Board Member',
	5: 'Secretary/C.E.O',
	6: 'Director HR',
	7: 'Legal Officer',
	8: 'Principal HR',
	9: 'HRM & D Officer',
	10: 'Legal Researcher',
	11: 'Records Management Officer',
}

def logout_redirect_uri(request):
	messages.success(request, f'Signed out successfully')
	id_token = request.session['oidc_id_token']
	logout_url = f'https://auth-cpsb.herokuapp.com/cpsb-openid/end-session/?id_token_hint={id_token}&post_logout_redirect_uri={settings.OIDC_POST_LOGOUT_REDIRECT_URI}'
	return logout_url

class AuthManager(OIDCAuthenticationBackend):
	def create_user(self, claims):
		user = super(AuthManager, self).create_user(claims)

		user.first_name = claims.get('given_name', '')
		user.last_name = claims.get('family_name', '')

		user.id_number = claims.get('id_number', '')
		user.user_type = claims.get('user_type', '')
		user.user_type_desc = USER_TYPE_CHOICES[user.user_type]

		user.date_of_birth = claims.get('user_profile')[0]['fields']['date_of_birth']
		user.gender = claims.get('user_profile')[0]['fields']['gender']

		user.save()

		general_group, created = Group.objects.get_or_create(name='ALL')
		staff_group, created = Group.objects.get_or_create(name='STAFF')

		if user.user_type in [0, 1]:
			admin_group = Group.objects.get(name='ADMIN')
			admin_group.user_set.add(user)
		elif user.user_type in [2,3,4,5]:
			board_members_group = Group.objects.get(name='BOARD_MEMBERS')
			board_members_group.user_set.add(user)
		elif user.user_type in [6,7,8,9,10,11]:
			secretariat_group = Group.objects.get(name='SECRETARIAT')
			secretariat_group.user_set.add(user)

		if user.user_type == 2:
			board_chairperson_group = Group.objects.get(name='BOARD_CHAIRPERSON')
			board_chairperson_group.user_set.add(user)

		if user.user_type == 5:
			secretary_group = Group.objects.get(name='SECRETARY')
			secretary_group.user_set.add(user)

		user.groups.add(general_group)
		user.groups.add(staff_group)

		return user

	def verify_claims(self, claims):
		verified = super(AuthManager, self).verify_claims(claims)
		id_number = claims.get('id_number')
		profile = claims.get('profile')
		return verified and id_number and profile