from django.urls import reverse
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test.utils import override_settings

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

import time


@override_settings(DEBUG=True)
class TestHomePage(StaticLiveServerTestCase):

    def setUp(self):
        """
        Initialize all set up for the tests
        """
        self.browser = webdriver.Chrome('functional_tests/chromedriver')

    def tearDown(self):
        """
        Closes the Browser after test is complete
        """
        self.browser.close()

    def test_anonymous_view_displayed_by_default(self):
        """
        Checks whether the anonymous view is displayed by default
        """
        self.browser.get(self.live_server_url)

        # When the user visits the URL by default, they should see a login link
        login_link = self.browser.find_element_by_id('login-btn')
        self.assertEquals(login_link.text, 'Login')

    def test_applicant_login_works(self):
        """
        Tests whether the applicant login link works
        """
        self.browser.get(self.live_server_url)
        self.browser.implicitly_wait(20)

        self.browser.find_element_by_id('login-btn').click()

        # The new URL should result to /accounts/login/ (reverse('account_login'))
        self.assertEquals(self.browser.current_url,
                          self.live_server_url + reverse('account_login'))

    def test_staff_login_works(self):
        """
        Tests whether the staff login works
        """
        self.browser.get(self.live_server_url)
        self.browser.implicitly_wait(20)

        self.browser.find_element_by_id('staff-login-btn').click()

        # The new URL should contain 'cpsb-openid'
        self.assertIn('cpsb-openid', self.browser.current_url)
