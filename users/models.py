from django.db import models
from django.contrib.auth.models import AbstractUser

GENDER = (
	('M', 'Male'),
	('F', 'Female'),
	('I', 'Intersex'),
)
# Create your models here.
class User(AbstractUser):
	user_type = models.PositiveSmallIntegerField(null=True)
	user_type_desc = models.CharField(max_length=40, null=True)
	other_names = models.CharField(null=True, blank=True, max_length=150)
	id_number = models.CharField(null=True, blank=True, max_length=10, unique=True)
	date_of_birth = models.DateField(null=True)
	gender = models.CharField(max_length=1, null=True, blank=True, choices=GENDER)
	is_applicant = models.BooleanField(default=False)
	phone = models.CharField(max_length=13, null=True, blank=True)

	class Meta:
		permissions = (
            ("can_add_technical_details", "Can add technical details"),
            ("has_staff_privileges", "Has staff privileges"),
            ("has_admin_privileges", "Has admin privileges")
            
        )

	def get_full_name(self):
		if self.first_name:
			return f'{self.first_name} {self.other_names} {self.last_name}'
		return self.id_number

	def __str__(self):
		return f'{self.first_name} {self.last_name}'