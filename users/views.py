from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib import messages
from django.views.generic import ListView
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from django.db.models import Count, Q

from formtools.wizard.views import SessionWizardView
from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView, BSModalReadView

from recruitment.models import Vacancy, Qualification, Employment
from recruitment.forms import (
	# Forms
	QualificationsFormSet,
	EmploymentFormSet, CurrentEmploymentForm
)
from recruitment.utils import create_wizard_formset_objects, create_wizard_form_objects, update_user_details

from .decorators import allowed_users
from .forms import ProfileForm, GroupForm, UserForm
from .models import User

# Create your views here.

def home(request):
	vacancies = Vacancy.objects.all()
	active_vacancies = Vacancy.objects.filter(status='active')
	active_vacancies_count = active_vacancies.count()

	context = {
		'title': 'Home',
		'active_vacancies': active_vacancies,
		'active_vacancies_count': active_vacancies_count, 
	}

	if request.user.has_perm('users.has_staff_privileges'):
		closed_vacancies = Vacancy.objects.filter(status='closed').annotate(
			    applicants=Count('applications'))
		shortlisted_vacancies = Vacancy.objects.filter(status='shortlisting_done').annotate(
			    shortlisted_candidates=Count('applications', filter=Q(applications__status='shortlisted')))
		finalized_vacancies = Vacancy.objects.filter(status='appointing_done').annotate(
			    appointed_candidates=Count('applications', filter=Q(applications__status='appointed')))
		
		context.update({
			'closed_vacancies': closed_vacancies,
			'closed_vacancies_count': closed_vacancies.count(),
			'shortlisted_vacancies': shortlisted_vacancies,
			'shortlisted_vacancies_count': shortlisted_vacancies.count(),
			'finalized_vacancies': finalized_vacancies,
			'finalized_vacancies_count': finalized_vacancies.count(),
			})

	if request.user.is_anonymous:
		return render(request, 'users/dashboard/anonymous.html', context)
	else:
		if request.user.groups.filter(name='ADMIN').exists():			
			return render(request, 'users/dashboard/admin.html', context)
		elif request.user.groups.filter(name='BOARD_MEMBERS').exists():
			return render(request, 'users/dashboard/board_members.html', context)
		elif request.user.groups.filter(name='SECRETARIAT').exists():
			return render(request, 'users/dashboard/staff.html', context) 
		elif request.user.is_applicant or request.user.user_type is None:
			if request.user.qualifications.all().exists():				
				applied_vacancies = request.user.applications.count()

				employment_records = request.user.employment_records.filter(designation__isnull=False, organization__isnull=False)
				current_employment_record = request.user.employment_records.filter(current=True)

				applicant_context = {
					
					'applied_vacancies': applied_vacancies,
					'employment_records': employment_records,
					'current_employment_record': current_employment_record
				}

				context.update(applicant_context)
				return render(request, 'users/dashboard/applicant.html', context)
			else:
				redirect_url = reverse('complete-profile')
				return redirect(f'{redirect_url}?next=/')

@login_required
def profile(request):
	form = ProfileForm(request.POST or None, instance=request.user)

	if form.is_valid():
		form.save()
		messages.success(request, 'Profile updated successfully')
		return redirect(request.META['HTTP_REFERER'])

	context = {
		'title': 'Profile',
		'form': form
	}

	if request.user.is_applicant:
		template_name = 'users/profile/applicant_profile.html'
	else:
		template_name = 'users/profile/staff_profile.html'
	return render(request, template_name, context)

class CompleteProfile(LoginRequiredMixin, SessionWizardView):
	form_list = [
		('profile', ProfileForm),
		('qualifications', QualificationsFormSet),
		('current_employment', CurrentEmploymentForm),
		('employment_history', EmploymentFormSet)
	]

	TEMPLATES = {
		'profile': 'users/profile/complete_profile.html',
		'qualifications': 'recruitment/forms/wizards/career/qualifications.html',
		'current_employment': 'recruitment/forms/wizards/career/current_employment.html',
		'employment_history': 'recruitment/forms/wizards/career/employment.html',
	}

	def get_template_names(self):
		return [self.TEMPLATES[self.steps.current]]

	def get_form(self, step=None, data=None, files=None):
		form = super().get_form(step, data, files)

		if step is None:
			step = self.steps.current		
		
		if step == 'qualifications':
			form.queryset = self.request.user.qualifications.all()
		elif step == 'employment_history':
			form.queryset = self.request.user.employment_records.all()
		return form

	def done(self, form_list, **kwargs):
		profile_cleaned_data = self.get_cleaned_data_for_step('profile')
		qualifications_cleaned_data = self.get_cleaned_data_for_step('qualifications')
		current_employment_cleaned_data = self.get_cleaned_data_for_step('current_employment')
		employment_cleaned_data = self.get_cleaned_data_for_step('employment_history')

		update_user_details(profile_cleaned_data, User, self.request.user)
		create_wizard_formset_objects(qualifications_cleaned_data, Qualification, self.request.user)
		create_wizard_form_objects(current_employment_cleaned_data, Employment, self.request.user)
		create_wizard_formset_objects(employment_cleaned_data, Employment, self.request.user)

		messages.success(self.request, 'Details Added Successfully')
		return redirect('home')

@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class GroupCreateView(BSModalCreateView):
	template_name = 'users/forms/modals/add_group.html'
	form_class = GroupForm
	success_message = 'Success: Group was added.'

	def get_success_url(self):
		return self.request.META.get('HTTP_REFERER', None)

@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class GroupListView(ListView):
	model = Group
	template_name = 'users/lists/groups.html'
	context_object_name = 'groups'

	def get_queryset(self):
		return Group.objects.exclude(name__in=['ALL', 'APPLICANTS'])

@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class AddGroupUser(BSModalReadView):
	template_name = 'users/forms/modals/group_user.html'
	model = Group

	def get_context_data(self, **kwargs):
	    context = super(AddGroupUser, self).get_context_data(**kwargs)
	    users = User.objects.filter(is_applicant=False)
	    non_members = [user for user in users if not user in self.object.user_set.all()]
	    context['users'] = non_members
	    return context

	def post(self, request, *args, **kwargs):
		user_ids = request.POST.getlist('user_ids')
		users = []
		for id_ in user_ids:
			user = User.objects.get(pk=id_)
			users.append(user)
			group = self.get_object()
			group.user_set.add(user)
		messages.success(self.request, f'Users {[user.first_name for user in users]} added to Group {group}')
		return redirect('group-list')

@method_decorator(allowed_users(['ADMIN']), name='dispatch')
class RemoveGroupUser(BSModalReadView):
	template_name = 'users/forms/modals/group_user.html'
	model = Group

	def get_context_data(self, **kwargs):
	    context = super(RemoveGroupUser, self).get_context_data(**kwargs)
	    context['users'] = self.object.user_set.filter(is_applicant=False)
	    return context

	def post(self, request, *args, **kwargs):
		user_ids = request.POST.getlist('user_ids')
		users = []
		for id_ in user_ids:
			user = User.objects.get(pk=id_)
			users.append(user)
			group = self.get_object()
			group.user_set.remove(user)
		messages.success(self.request, f'Users {[user.first_name for user in users]} removed from Group {group}')
		return redirect('group-list')
