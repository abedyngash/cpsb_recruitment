from django.urls import path, include
from .views import (
	home, profile, CompleteProfile, 
	GroupCreateView, GroupListView, 
	AddGroupUser, RemoveGroupUser
)
urlpatterns = [
    path('', home, name='home'),
    path('users/profile/', profile, name='profile'),
    path('users/complete-profile/', CompleteProfile.as_view(), name='complete-profile'),
    path('users/groups/add/', GroupCreateView.as_view(), name='add-group'),
    path('users/groups/lists/', GroupListView.as_view(), name='group-list'),
    path('users/groups/<int:pk>/members/add/', AddGroupUser.as_view(), name='add-group-member'),
    path('users/groups/<int:pk>/members/remove/', RemoveGroupUser.as_view(), name='remove-group-member')
]
