from django.shortcuts import redirect
from django.contrib import messages

def allowed_users(allowed_roles=[]):
	def decorator(view_func):
		def wrapper_func(request, *args, **kwargs):
			user_groups = request.user.groups.all()
			user_groups_names = [group.name for group in user_groups]
			check = any(item in user_groups_names for item in allowed_roles)

			if check:
				return view_func(request, *args, **kwargs)
			else:
				messages.error(request, f'You are not allowed to access that page')
				return redirect(request.META.get('HTTP_REFERER') or 'home')
		return wrapper_func
	return decorator